import React, { Component } from 'react';

var students = [{
  id: 1,
  name: "Addy Osmani",
  twitter: "@addyosmani"
}, {
  id: 2,
  name: "Paul Irish",
  twitter: "@paulrirish"
}, {
  id: 3,
  name: "Ilya Grigorik",
  twitter: "@grigorik"
}, {
  id: 4,
  name: "Eric Elliot",
  twitter: "@ericelliot"
}, {
  id: 5,
  name: "Richard Hendriks",
  twitter: "richi"
}, {
  id: 6,
  name: "Big Head",
  twitter: "@bighead"
}];

class Student extends Component {
  getTwitterUsername(studentId) {
    var found = students.filter(s => s.id === parseInt(studentId));

    return found[0].twitter;
  }

  render() {
    console.log(this)
    var { studentId } = this.props.match.params;

    return (
      <h2>Twitter: <strong>{this.getTwitterUsername(studentId)}</strong></h2>
    );
  }
}

export default Student;
