import React, { Component } from 'react';

import Home from "./Home";
import About from "./About";
import Portfolio from "./Portfolio";
import NoMatch from "./NoMatch";
import Cohort from "./Cohort";
import Student from "./Student";

import { Route, Link, Switch } from "react-router-dom";

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <ul>
          <li><Link to="/">Home</Link></li> {/*to brings the url, thats the url */}
          <li><Link to="/about">About</Link></li>
          <li><Link to="/portfolio">Portfolio</Link></li>
          <li><Link to="/cohort">Cohort</Link></li>
        </ul>

        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/about" exact component={About} />
          <Route path="/portfolio" exact component={Portfolio} />
          <Route path="/cohort" exact component={Cohort} />
          <Route path="/students/:studentId" component={Student} /> {/*this is the route that you declare, studentId can be anything */}
          <Route component={NoMatch} />
        </Switch>
      </React.Fragment>
    );
  }
}

export default App;


// switch choose the first one that makes match and return that, renderize the first match, it means that if theres 2 with the same name will return the first one
// Routes defines what is seeng of the component, link is only like the anchor at html, just get you where you wanna go
// switch only recives components
//path="srudewnts/:studentId <= this is a param (you can see it at the console with console.log(this) and this will show everything)
