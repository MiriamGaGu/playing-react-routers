import React, { Component } from 'react';

import { Link } from "react-router-dom";

var students = [{
  id: 1,
  name: "Addy Osmani",
  twitter: "@addyosmani"
}, {
  id: 2,
  name: "Paul Irish",
  twitter: "@paulrirish"
}, {
  id: 3,
  name: "Ilya Grigorik",
  twitter: "@grigorik"
}, {
  id: 4,
  name: "Eric Elliot",
  twitter: "@ericelliot"
}, {
  id: 5,
  name: "Richard Hendriks",
  twitter: "richi"
}, {
  id: 6,
  name: "Big Head",
  twitter: "@bighead"
}];

class Cohort extends Component {
  render() {
    return (
      <ul>
        { students.map(s => {
          return (
            <li key={ s.id }>
              <Link to={ "/students/" + s.id }>{ s.name }</Link>
            </li>
          );
        }) }
      </ul>
    );
  }
}

export default Cohort;
