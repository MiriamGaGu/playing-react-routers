import React, { Component } from 'react';

import { Link } from "react-router-dom";

class NoMatch extends Component {
  render() {
    return (
      <React.Fragment>
        <h2>Route not found</h2>
        <Link to="/">Go to home</Link>
      </React.Fragment>
    );
  }
}

export default NoMatch;
